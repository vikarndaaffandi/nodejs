var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    // res.render('index', { title: 'Express' });
    var $result = {};
    $result['responseCode'] = '00';
    $result['responseMessage'] = 'Success';
    $result['responseData'] = {};

    var $data = [];
    $data.push({ '1': 'Xylophone' });
    $data.push({ '2': 'Carrot' });
    $result['responseData']['data'] = $data;

    res.setHeader('Content-Type', 'application/json');
    res.send($result);
});

module.exports = router;
