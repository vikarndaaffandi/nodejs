var express = require('express');
var connection = require('../config/./db.js');
var router = express.Router();
var response = require('../lib/./response.js');

/* GET customer listing. */
router.get('/', function(req, res, next) {
    var data = [];
    connection.connect();

    connection.query('SELECT * FROM mahasiswa', function (error, rows, fields) {
        if (error) throw error;
        response['responseData']['data_mahasiswa'] = rows;

        connection.query('SELECT * FROM mata_kuliah', function (error, rows, fields) {
            if (error) throw error;
            response['responseData']['data_mata_kuliah'] = rows;
            connection.end();
            res.json(response);
        });
    });
});

/* create new mahasiswa */
router.post('/create',function(req, res, next) {
    var npm = req.body.npm;
    var nama = req.body.nama;
    connection.query('INSERT INTO mahasiswa (`npm`,`nama`) VALUES (?,?)', [npm,nama], function (error, rows, fields) {
        if (error) throw error;
        connection.end();
        res.send(response);
    });
});

module.exports = router;
